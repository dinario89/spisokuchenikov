//
//  ViewController.swift
//  SpisokUchenikov
//
//  Created by  Динар on 28/04/2019.
//  Copyright © 2019  Динар. All rights reserved.
//

import UIKit

class StudentsViewController: UIViewController {

    var idCell = "StudentTableViewCellID"
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addBarButtonItem: UIBarButtonItem!
    
    var students: [Student] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        loadStudents()
        
        tableView.reloadData()
    }
    
    func loadStudents() {
        
        let jsonDecoder = JSONDecoder()
        
        students = []
        
        if let arrayJsonData = UserDefaults.standard.array(forKey: "Students") as?[String] {
            for jsonDataString in arrayJsonData {
                do{
                    students.append(try jsonDecoder.decode(Student.self, from: jsonDataString.data(using: String.Encoding.utf8)!))
                } catch {
                    
                }
            }
        } else {
            students = []
        }
    }
    
}

//MARK: - TableView

extension StudentsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return students.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: StudentTableViewCell = tableView.dequeueReusableCell(withIdentifier: idCell) as! StudentTableViewCell
        
        let item = students[indexPath.row]
        
        cell.nameLabel.text = String.localizedStringWithFormat("%@ %@", item.surname, item.name)
        cell.rateLabel.text =  String.localizedStringWithFormat("%d", item.avgRate)
        
        return cell
        
    }
    
}

extension StudentsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "EditorTableViewControllerID") as? EditorTableViewController
        
        vc?.studentId = students[indexPath.row].id
        
        navigationController?.pushViewController(vc!, animated: true)
    }
    
//    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
//        if editingStyle == .delete {
//            print("Deleted")
//
//            self.catNames.remove(at: indexPath.row)
//            self.tableView.deleteRows(at: [indexPath], with: .automatic)
//        }
//    }
    
}

