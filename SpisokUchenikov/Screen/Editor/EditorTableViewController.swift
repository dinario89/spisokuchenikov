//
//  EditorViewController.swift
//  SpisokUchenikov
//
//  Created by  Динар on 07/05/2019.
//  Copyright © 2019  Динар. All rights reserved.
//

import UIKit

class EditorTableViewController: UITableViewController {
    
    var studentId: String = ""
    
    var student: Student?
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var surnameTextField: UITextField!
    @IBOutlet weak var avgRateTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loadStudents()
        
        nameTextField.text = student?.name
        surnameTextField.text = student?.surname
        avgRateTextField.text = String(student?.avgRate ?? 0)
        
        // Do any additional setup after loading the view.
    }

    @IBAction func actionCancel(_ sender: Any) {
        closeViewController()
    }
    @IBAction func actionSave(_ sender: Any) {
        saveStudents()
        closeViewController()
    }
    
    func closeViewController() {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    func loadStudents() {
        
        let jsonDecoder = JSONDecoder()
        
        if let arrayJsonData = UserDefaults.standard.array(forKey: "Students") as?[String] {
            
            var students: [Student] = [];
            for jsonDataString in arrayJsonData {
            
                do{
                    students.append(try jsonDecoder.decode(Student.self, from: jsonDataString.data(using: String.Encoding.utf8)!))
                    
                } catch {
                    
                }
            }
            student = students.first(where: { $0.id == studentId })
        } else {
            student = Student.init()
        }
    }
    
    func saveStudents() {
        
        let jsonDecoder = JSONDecoder()
        
        if let arrayJsonData = UserDefaults.standard.array(forKey: "Students") as?[String] {
            
            var students: [Student] = [];
            for jsonDataString in arrayJsonData {
                
                do{
                    students.append(try jsonDecoder.decode(Student.self, from: jsonDataString.data(using: String.Encoding.utf8)!))
                    
                } catch {
                    
                }
            }
            
            var student = students.first(where: { $0.id == studentId })
            
            if(student != nil) {
                
                student?.name = nameTextField.text ?? ""
                student?.surname = surnameTextField.text ?? ""
                student?.avgRate = Int(avgRateTextField.text ?? "0") ?? 0
                
            } else {
                
                let student = Student.init(
                    name: nameTextField.text ?? "",
                    surname: surnameTextField.text ?? "",
                    avgRate: Int(avgRateTextField.text ?? "0") ?? 0)
                
                students.append(student)
                
            }
            
            saveStudents(students: students)
            
        } else {
            
            var array: [Student] = []
            
            let student = Student.init(
                name: nameTextField.text ?? "",
                surname: surnameTextField.text ?? "",
                avgRate: Int(avgRateTextField.text ?? "0") ?? 0)
            
            array.append(student)
            
            saveStudents(students: array)
            
        }
    
    }
    
    func saveStudents(students: [Student]) {
        
        let jsonEncoder = JSONEncoder()
        
        do {
            var jsonDataArray:[String] = []
            
            for student in students {
                
                let jsonData = try jsonEncoder.encode(student)
                
                let json = String(data: jsonData, encoding: String.Encoding.utf8)
                
                jsonDataArray.append(json!)
                
            }
            
            
            UserDefaults.standard.set(jsonDataArray, forKey: "Students")
            UserDefaults.standard.synchronize()
        } catch {
            print(error)
        }
        
    }

}
