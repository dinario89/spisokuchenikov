//
//  Student.swift
//  SpisokUchenikov
//
//  Created by  Динар on 07/05/2019.
//  Copyright © 2019  Динар. All rights reserved.
//

import Foundation

class Student: Codable {
    
    var id: String
    var name: String
    var surname: String
    var avgRate: Int
    
    init() {
        self.id = UUID.init().uuidString
        self.name = ""
        self.surname = ""
        self.avgRate = 0
    }
    
    init(name: String, surname: String, avgRate: Int) {
        self.id = UUID.init().uuidString
        self.name = name
        self.surname = surname
        self.avgRate = avgRate
    }
}

